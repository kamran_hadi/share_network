package com.sharenetwork;

import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import com.sharenetwork.manager.HotspotManager;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Enumeration;

/**
 * Wifi Utility class
 * Created by Kamran on 10/1/17.
 */

public class WifiUtils {
    private static final String TAG = "WifiUtils";
    public static final String SENDER_WIFI_NAMING_SALT = "stl";

    public static String getDeviceName(WifiManager wifiManager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG, "For version >= MM inaccessible mac - falling back to the default device name: " + HotspotManager.DEFAULT_DEVICE_NAME);
            return HotspotManager.DEFAULT_DEVICE_NAME;
        }
        String macString = wifiManager.getConnectionInfo().getMacAddress();
        if (macString == null) {
            Log.d(TAG, "MAC Address not found - Wi-Fi disabled? Falling back to the default device name: " + HotspotManager.DEFAULT_DEVICE_NAME);
            return HotspotManager.DEFAULT_DEVICE_NAME;
        }
        byte[] macBytes = Utils.macAddressToByteArray(macString);

        try {
            Enumeration<NetworkInterface> ifaces = NetworkInterface.getNetworkInterfaces();
            while (ifaces.hasMoreElements()) {
                NetworkInterface iface = ifaces.nextElement();

                byte[] hardwareAddress = iface.getHardwareAddress();
                if (hardwareAddress != null && Arrays.equals(macBytes, hardwareAddress)) {
                    return iface.getName();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "exception in retrieving device name: " + e.getMessage());
        }

        Log.w(TAG, "None found - falling back to the default device name: " + HotspotManager.DEFAULT_DEVICE_NAME);
        return HotspotManager.DEFAULT_DEVICE_NAME;
    }

    public static String getHostIpAddress() {
        try {
            for (final Enumeration<NetworkInterface> enumerationNetworkInterface = NetworkInterface.getNetworkInterfaces(); enumerationNetworkInterface.hasMoreElements(); ) {
                final NetworkInterface networkInterface = enumerationNetworkInterface.nextElement();
                for (Enumeration<InetAddress> enumerationInetAddress = networkInterface.getInetAddresses(); enumerationInetAddress.hasMoreElements(); ) {
                    final InetAddress inetAddress = enumerationInetAddress.nextElement();
                    final String ipAddress = inetAddress.getHostAddress();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return ipAddress;
                    }
                }
            }
            return null;
        } catch (final Exception e) {
            Log.e("WifiUtils", "exception in fetching inet address: " + e.getMessage());
            return null;
        }
    }
}
