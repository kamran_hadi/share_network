package com.sharenetwork;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.sharenetwork.manager.HotspotManager;
import com.sharenetwork.services.ShareNetworkService;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.Arrays;

import static com.sharenetwork.CreateHotspotActivity.ShareUIHandler.UPDATE_AP_STATUS;

public class CreateHotspotActivity extends AppCompatActivity {

    private static final String TAG = "CreateHotspotActivity";

    private CompoundButton.OnCheckedChangeListener m_sender_ap_switch_listener;
    SwitchCompat m_apControlSwitch;
    TextView m_sender_wifi_info;

    private static final int REQUEST_WRITE_SETTINGS = 1;
    public static final String PREFERENCES_KEY_DATA_WARNING_SKIP = "sharethem_data_warning_skip";
    private HotspotManager hotspotManager;
    private boolean isApEnabled = false;
    private boolean shouldAutoConnect = true;
    private String[] m_sharedFilePaths = null;
    private ShareUIHandler m_uiUpdateHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_hotspot);
        findViews();
        setSwitchListener();

        hotspotManager = HotspotManager.getInstance(getApplicationContext());
        m_apControlSwitch.setOnCheckedChangeListener(m_sender_ap_switch_listener);

    }

    private void findViews() {
        m_sender_wifi_info = (TextView) findViewById(R.id.p2p_sender_wifi_hint);
        m_apControlSwitch = (SwitchCompat) findViewById(R.id.p2p_sender_ap_switch);
    }

    private void setSwitchListener() {
        m_sender_ap_switch_listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                shouldAutoConnect = isChecked;
                if (isChecked) {
                    //If target version is MM and beyond, you need to check System Write permissions to proceed.
                    if (Build.VERSION.SDK_INT >= 23 &&
                            // if targetSdkVersion >= 23
                            //     ShareActivity has to check for System Write permissions to proceed
                            Utils.getTargetSDKVersion(getApplicationContext()) >= 23 && !Settings.System.canWrite(CreateHotspotActivity.this)) {
                        changeApControlCheckedStatus(false);
                        showMessageDialogWithListner(getString(R.string.p2p_sender_system_settings_permission_prompt), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                                intent.setData(Uri.parse("package:" + getPackageName()));
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivityForResult(intent, REQUEST_WRITE_SETTINGS);
                            }
                        }, false, true);
                        return;
                    } else if (!getSharedPreferences(getPackageName(), Context.MODE_PRIVATE).getBoolean(PREFERENCES_KEY_DATA_WARNING_SKIP, false) && Utils.isMobileDataEnabled(getApplicationContext())) {
                        changeApControlCheckedStatus(false);
                        showDataWarningDialog();
                        return;
                    }
                    m_sender_wifi_info.setText(getString(R.string.p2p_sender_hint_connecting));
                    enableAp();
                } else {
                    changeApControlCheckedStatus(true);
                    showMessageDialogWithListner(getString(R.string.p2p_sender_close_warning), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d(TAG, "sending intent to service to stop p2p..");
                            resetSenderUi(true);//false to close switch
                        }
                    }, true, false);
                }
            }
        };
    }

    //region: Dialog utilities
    public void showMessageDialogWithListner(String message,
                                             DialogInterface.OnClickListener listner, boolean showNegavtive,
                                             final boolean finishCurrentActivity) {
        if (isFinishing())
            return;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        //noinspection deprecation
        builder.setMessage(Html.fromHtml(message));
        builder.setPositiveButton("OK", listner);
        if (showNegavtive)
            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (finishCurrentActivity)
                                finish();
                            else
                                dialog.dismiss();
                        }
                    });
        builder.show();
    }

    public void showDataWarningDialog() {
        if (isFinishing())
            return;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setCancelable(false);
        builder.setMessage(getString(R.string.sender_data_on_warning));
        builder.setPositiveButton(getString(R.string.label_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(
                        Settings.ACTION_SETTINGS));
            }
        });
        builder.setNegativeButton(getString(R.string.label_thats_ok),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        changeApControlCheckedStatus(true);
                        m_sender_wifi_info.setText(getString(R.string.p2p_sender_hint_connecting));
                        enableAp();
                    }
                });
        builder.setNeutralButton(getString(R.string.label_dont_ask), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
                prefs.edit().putBoolean(PREFERENCES_KEY_DATA_WARNING_SKIP, true).apply();
                changeApControlCheckedStatus(true);
                m_sender_wifi_info.setText(getString(R.string.p2p_sender_hint_connecting));
                enableAp();
            }
        });
        builder.show();
    }

    private void enableAp() {

        startP2pSenderWatchService();
        refreshApData();
//        m_receivers_list_layout.setVisibility(View.VISIBLE);
    }

    /**
     * Starts {@link ShareNetworkService} with intent action {@link ShareNetworkService#WIFI_AP_ACTION_START} to enableShareThemHotspot Hotspot and start {SHAREthemServer}.
     */
    private void startP2pSenderWatchService() {
        m_sharedFilePaths = new String[]{"filePath"};
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareNetworkService.class);
        p2pServiceIntent.putExtra(ShareNetworkService.EXTRA_FILE_PATHS, m_sharedFilePaths);
        if (null != getIntent()) {
            p2pServiceIntent.putExtra(ShareNetworkService.EXTRA_PORT, ShareNetworkService.PORT_NUMBER_TEMP);
            p2pServiceIntent.putExtra(ShareNetworkService.EXTRA_SENDER_NAME, "KAMRAN");
        }
        p2pServiceIntent.setAction(ShareNetworkService.WIFI_AP_ACTION_START);
        startService(p2pServiceIntent);
    }

    /**
     * Calls methods - {@link CreateHotspotActivity#updateApStatus()} & {CreateHotspotActivity#listApClients()} which are responsible for displaying Hotpot information and Listing connected clients to the same
     */
    private void refreshApData() {
        if (null == m_uiUpdateHandler)
            m_uiUpdateHandler = new ShareUIHandler(this);
        updateApStatus();
//        listApClients();
    }

    private void disableAp() {
        //Send STOP action to service
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareNetworkService.class);
        p2pServiceIntent.setAction(ShareNetworkService.WIFI_AP_ACTION_STOP);
        startService(p2pServiceIntent);
        isApEnabled = false;
    }

    private void resetSenderUi(boolean disableAP) {
//        m_uiUpdateHandler.removeCallbacksAndMessages(null);
        m_sender_wifi_info.setText(getString(R.string.p2p_sender_hint_text));
        if (disableAP)
            disableAp();
        else {
            changeApControlCheckedStatus(false);
        }

        changeApControlCheckedStatus(false);
    }

    /**
     * Changes checked status without invoking listener. Removes @{@link android.widget.CompoundButton.OnCheckedChangeListener} on @{@link SwitchCompat} button before changing checked status
     *
     * @param checked if <code>true</code>, sets @{@link SwitchCompat} checked.
     */
    private void changeApControlCheckedStatus(boolean checked) {
        m_apControlSwitch.setOnCheckedChangeListener(null);
        m_apControlSwitch.setChecked(checked);
        m_apControlSwitch.setOnCheckedChangeListener(m_sender_ap_switch_listener);
    }

    private void createCustomHotspot() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(android.content.Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }

        WifiConfiguration netConfig = new WifiConfiguration();

        netConfig.SSID = "KamranApp";
        netConfig.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        netConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        netConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        try {
            Method setWifiApMethod = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
            boolean apstatus = (Boolean) setWifiApMethod.invoke(wifiManager, netConfig, true);

            Method isWifiApEnabledmethod = wifiManager.getClass().getMethod("isWifiApEnabled");
            while (!(Boolean) isWifiApEnabledmethod.invoke(wifiManager)) {
                Boolean status = (Boolean) isWifiApEnabledmethod.invoke(wifiManager);
                Log.d(TAG, "while: " + status);
            }

            Method getWifiApStateMethod = wifiManager.getClass().getMethod("getWifiApState");
            int apstate = (Integer) getWifiApStateMethod.invoke(wifiManager);
            Method getWifiApConfigurationMethod = wifiManager.getClass().getMethod("getWifiApConfiguration");
            netConfig = (WifiConfiguration) getWifiApConfigurationMethod.invoke(wifiManager);
            Log.e(TAG, "\nSSID:" + netConfig.SSID + "\nPassword:" + netConfig.preSharedKey + "\n");

        } catch (Exception e) {
            Log.e(TAG, e.getMessage() + " " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void onCreateHotspotClick(View view) {
        createCustomHotspot();

    }

    static class ShareUIHandler extends Handler {
        WeakReference<CreateHotspotActivity> mActivity;

        static final int LIST_API_CLIENTS = 100;
        static final int UPDATE_AP_STATUS = 101;

        ShareUIHandler(CreateHotspotActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            CreateHotspotActivity activity = mActivity.get();
            if (null == activity || msg == null || !activity.m_apControlSwitch.isChecked())
                return;
            if (msg.what == LIST_API_CLIENTS) {
//                activity.listApClients();
                Log.d(TAG,"LIST_API_CLIENTS "+LIST_API_CLIENTS);
            } else if (msg.what == UPDATE_AP_STATUS) {
                activity.updateApStatus();
            }
        }
    }

    /**
     * Updates Hotspot configuration info like Name, IP if enabled.<br> Posts a message to {@link ShareUIHandler} to call itself every 1500ms
     */
    private void updateApStatus() {
        if (!HotspotManager.isSupported()) {
            m_sender_wifi_info.setText("Warning: Hotspot mode not supported!\n");
        }
        if (hotspotManager.isEnabled()) {
            if (!isApEnabled) {
                isApEnabled = true;
                startHostspotCheckOnService();
            }
            WifiConfiguration config = hotspotManager.getConfiguration();
            String ip = Build.VERSION.SDK_INT >= 23 ? WifiUtils.getHostIpAddress() : hotspotManager.getHostIpAddress();
            if (TextUtils.isEmpty(ip))
                ip = "";
            else
                ip = ip.replace("/", "");
            m_sender_wifi_info.setText(getString(R.string.p2p_sender_hint_wifi_connected, config.SSID, "http://" + ip + ":" + hotspotManager.getShareServerListeningPort()));

        }
        if (null != m_uiUpdateHandler) {
            m_uiUpdateHandler.removeMessages(UPDATE_AP_STATUS);
            m_uiUpdateHandler.sendEmptyMessageDelayed(UPDATE_AP_STATUS, 1500);
        }
    }

    /**
     * Starts {@link ShareNetworkService} with intent action {@link ShareNetworkService#WIFI_AP_ACTION_START_CHECK} to make {@link ShareNetworkService} constantly check for Hotspot status. (Sometimes Hotspot tend to stop if stayed idle for long enough. So this check makes sure {@link ShareNetworkService} is only alive if Hostspot is enaled.)
     */
    private void startHostspotCheckOnService() {
        Intent p2pServiceIntent = new Intent(getApplicationContext(), ShareNetworkService.class);
        p2pServiceIntent.setAction(ShareNetworkService.WIFI_AP_ACTION_START_CHECK);
        startService(p2pServiceIntent);
    }
}
