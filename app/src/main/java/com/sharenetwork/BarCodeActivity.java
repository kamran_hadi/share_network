package com.sharenetwork;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.net.NetworkInterface;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BarCodeActivity extends AppCompatActivity {

    private static final String TAG = "BarCodeActivity";
    private TextView ssidTextView;
    private EditText ssidEditText, passwordEditText;
    private ImageView barcodeImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_code);
        ssidTextView = (TextView) findViewById(R.id.ssid_text);
        ssidEditText = (EditText) findViewById(R.id.ssid_editText);
        passwordEditText = (EditText) findViewById(R.id.password_editText);
        barcodeImage = (ImageView) findViewById(R.id.barcode_imageView);
    }

    public void onGetWifiDataClick(View view) {
        String data = "";
        String pas = "";
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration conf : configuredNetworks) {
            data += conf.SSID.replace("\"", "") + " , ";
//            data += "SSID: "+conf.SSID+" PASS: "+conf.preSharedKey;
        }
        ssidTextView.setText(data);
        ssidEditText.setText(getWifiData().replace("\"", ""));
    }

    public String getWifiData() {
        String wifiData = null;
        WifiManager manager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (manager.isWifiEnabled()) {
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (wifiInfo != null) {
                NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(wifiInfo.getSupplicantState());
                if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                    wifiData = "SSID: " + wifiInfo.getSSID();
                    wifiData += "\nBSSID: " + wifiInfo.getBSSID();
                    wifiData += "\nHiddenSSID: " + wifiInfo.getHiddenSSID();
                    wifiData += "\nMacAddress: " + wifiInfo.getMacAddress();
                    wifiData += "\nIpAddress: " + wifiInfo.getIpAddress();
                    wifiData += "\nNetworkId: " + wifiInfo.getNetworkId();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        wifiData += "\nFrequency: " + wifiInfo.getFrequency();
                        wifiData += "\nNetworkType: " + getNetworkType(wifiInfo.getFrequency());
                    }
//                    return wifiData;
                    return wifiInfo.getSSID();
                }
            }
        }
        return "NA";
    }

    public static String getWifiMacAddress(WifiInfo wifiInfo) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return wifiInfo.getMacAddress();
            }

            String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase(interfaceName)) {
                    continue;
                }

                byte[] mac = intf.getHardwareAddress();
                if (mac == null) {
                    return "";
                }

                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length() > 0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage() + " " + Arrays.toString(ex.getStackTrace()));
        } // for now eat exceptions
        return "";
    }

    public String getNetworkType(int freq) {
        if (freq > 4900 && freq < 5900)
            return "5G";
        if (freq > 2400 && freq < 2500)
            return "2.4G";
        else
            return "Unknown";
    }

    public void onConnectClick(View view) {
        String message;
        String ssId = ssidEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (isEmpty(ssId)) {
            message = "SSID is Blank";
            showToast(message);
            return;
        } else if (isEmpty(password)) {
            message = "Password is Blank";
            showToast(message);
            return;
        }
        String invertedComma = "\"";
        String str = "{\"name\":\"apple\",\"email_id\":\"apple@apple.com\"}";

        generateBarCode("{\"ssId\":" + invertedComma + ssId + invertedComma + ",\"password\":" + invertedComma + password + invertedComma + "}");
//        connectToWifi(ssId, password);

    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean isEmpty(String data) {
        return data == null || data.trim().length() <= 0;

    }

    public void connectToWifi(String ssId, String password) {
        try {
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(android.content.Context.WIFI_SERVICE);
            WifiConfiguration wc = new WifiConfiguration();
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            wc.SSID = "\"" + ssId + "\"";
            wc.preSharedKey = "\"" + password + "\"";
            wc.status = WifiConfiguration.Status.ENABLED;
            wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wifiManager.setWifiEnabled(true);
            int netId = wifiManager.addNetwork(wc);
            if (netId == -1) {
                netId = getExistingNetworkId(ssId);
            }
            wifiManager.disconnect();
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getExistingNetworkId(String SSID) {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        if (configuredNetworks != null) {
            for (WifiConfiguration existingConfig : configuredNetworks) {
                if (existingConfig.SSID.equals(SSID)) {
                    return existingConfig.networkId;
                }
            }
        }
        showToast(SSID + ": not in range");
        return -1;
    }

    private void generateBarCode(String barcodeData) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(barcodeData, BarcodeFormat.QR_CODE, 800, 800);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            barcodeImage.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public void onShareHotspot(View view) {
        Intent intent = new Intent(this,CreateHotspotActivity.class);
        startActivity(intent);
    }
}
