package com.sharenetwork;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

/**
 * App Utility Class
 * Created by Kamran on 10/1/17.
 */

public class Utils {

    public static byte[] macAddressToByteArray(String macString) {
        String[] mac = macString.split("[:\\s-]");
        byte[] macAddress = new byte[6];
        for (int i = 0; i < mac.length; i++) {
            macAddress[i] = Integer.decode("0x" + mac[i]).byteValue();
        }
        return macAddress;
    }

    public static int getTargetSDKVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.applicationInfo.targetSdkVersion;
        } catch (PackageManager.NameNotFoundException nnf) {
            return -1;
        }
    }

    public static boolean isMobileDataEnabled(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), "mobile_data", 1) == 1;
        /*return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 ?
                Settings.Global.getInt(context.getContentResolver(), "mobile_data", 1) == 1 :
                Settings.Secure.getInt(context.getContentResolver(), "mobile_data", 1) == 1;*/
    }
}
